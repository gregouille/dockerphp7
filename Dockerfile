FROM php:8.1.0RC5-apache

ENV PHP_SECURITY_CHECHER_VERSION=1.0.0

RUN apt-get update && apt-get install -y \
      wget \
      git \
      fish

RUN apt-get update && apt-get install -y libzip-dev libicu-dev && docker-php-ext-install pdo pdo_mysql zip intl opcache


# Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer



# Xdebug (disabled by default, but installed if required)
# RUN pecl install xdebug-2.9.7 && docker-php-ext-enable xdebug
ADD php.ini /usr/local/etc/php/conf.d/

WORKDIR /var/www

EXPOSE 80
EXPOSE 443
EXPOSE 9000